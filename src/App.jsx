import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'
import moviesData from './../movies.json'
import Movies from './Components/Movies'

function App() {
  // const [count, setCount] = useState(0)
  // console.log(moviesData)

  let modifiedData = moviesData.map((data) => {
    data['status'] = 'want to watch'
    return data;
  })

  const [movieData, setMovieData] = useState([modifiedData])

  return (
    <>
      <Movies modifiedData={modifiedData} setMovieData={setMovieData}  />
    </>
  )
}

export default App
