import React, { useState } from 'react'

export default function Movies({ modifiedData , setMovieData}) {


    // console.log(modifiedData)


    const ModifiedMovieData = modifiedData.reduce((Movies, data) => {
        Movies[data.status].push(data);
        return Movies;
    }, {
        'want to watch': [],
        'watched': [],
        'watching': []
    });

    function statusChange(selectedStatus, data) {
       console.log(selectedStatus)
       console.log(data)

       const updatedMovieData = modifiedData.filter(movie => {

        console.log(movie)
        
       return movie.data !== data;
    });

    setMovieData(updatedMovieData);

    }

    return (
        <>
            <div>
                <h1>Want to Watch</h1>
                <div style={{ display: 'flex', gap: '20px', flexWrap: 'wrap' }}>
                    {ModifiedMovieData['want to watch'].map(data => (
                        <div className='Movie' style={{ width: '200px'}}>

                            <h1>{data.Title}</h1>
                            <h4>{data.Rating}</h4>
                            <h4>{data.Genre}</h4>
                            <select onChange={(event) => statusChange(event.target.value, data)}>
                                <option value="want to watch">want to watch</option>
                                <option value="watching">watching</option>
                                <option value="watched">watched</option>
                            </select>
                          
                        </div>
                    ))}
                </div>
            </div>

            <div>
                <h1>Watched</h1>
                <div style={{ display: 'flex', gap: '20px', flexWrap: 'wrap' }}>
                    {ModifiedMovieData['watched'].map(data => (
                        <div className='Movie' style={{ width: '200px' }}>
                            <h1>{data.Title}</h1>
                            <h4>{data.Rating}</h4>
                            <h4>{data.Genre}</h4>

                            <select onChange={(event) => statusChange(event.target.value, data)}>
                                <option value="want to watch">want to watch</option>
                                <option value="watching">watching</option>
                                <option value="watched">watched</option>
                            </select>
                        </div>
                    ))}
                </div>
            </div>

            <div>
                <h1>Watching</h1>
                <div style={{ display: 'flex', gap: '20px', flexWrap: 'wrap' }}>
                    {ModifiedMovieData['watching'].map(data => (
                        <div className='Movie' style={{ width: '200px' }}>
                            <h1>{data.Title}</h1>
                            <h4>{data.Rating}</h4>
                            <h4>{data.Genre}</h4>

                            <select onChange={(event) => statusChange(event.target.value, data)}>
                                <option value="want to watch">want to watch</option>
                                <option value="watching">watching</option>
                                <option value="watched">watched</option>
                            </select>
                        </div>
                    ))}
                </div>
            </div>

        </>
    )
}
